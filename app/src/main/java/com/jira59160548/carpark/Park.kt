package com.jira59160548.carpark

class Park(var license: String = "ว่าง", var brand: String = "", var ownerName: String = "") {
    fun clearPark() {
        this.license = "ว่าง"
        this.brand = ""
        this.ownerName = ""
    }

    override fun toString(): String {
        return "Park(license='$license', brand='$brand', ownerName='$ownerName')"
    }


}